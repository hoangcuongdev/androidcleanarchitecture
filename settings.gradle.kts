val modules = setOf(":app", ":utils", ":core")

rootProject.name = "Android Clean Architecture"
// include(*ModuleDependency.getAllModules().toTypedArray())
    include(*modules.toTypedArray())

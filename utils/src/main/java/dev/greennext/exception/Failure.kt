package dev.greennext.exception

sealed class Failure {

    /** * Extend this class for feature specific failures.*/
    abstract class FeatureFailure : Failure()
}

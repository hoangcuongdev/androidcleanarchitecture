@file:JvmName("StringEx")
package dev.greennext.utils

/**
 * Extension method to check if String is Email.
 */
fun String.isEmail(): Boolean {
    val p = "^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w+)+)\$".toRegex()
    return matches(p)
}

/**
 * Extension method to check if String is Number.
 */
fun String.isNumeric(): Boolean {
    val p = "^[0-9]+$".toRegex()
    return matches(p)
}

fun String.passwordIsValid(): Boolean {
    val rx = "^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,30}$".toRegex()
    return matches(rx)
}

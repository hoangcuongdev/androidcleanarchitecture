@file:JvmName("Standard")
inline fun <T, R> T?.let(notNull: (T) -> R, ifNull: () -> R): R {
    return if (this != null) notNull.invoke(this) else ifNull.invoke()
}

inline fun <T> T?.also(notNull: (T) -> Unit, ifNull: () -> Unit): T? {
    if (this != null) notNull.invoke(this) else ifNull.invoke()
    return this
}

/**
 * If `this` value is null calls [block] and returns its result T else returns `this` value.
 */
inline fun <T> T?.ifNull(block: () -> T): T {
    return this ?: block.invoke()
}

/**
 * If `this` value is null returns non null value T else returns `this` value.
 * Use this function when the value arg is already available.
 * If value needs to be calculated use ifNull(block: () -> T) version
 *
 * Bad: someNullHexString.ifNull(someByteArray.toHexString())
 *
 * Good: someNullHexString.ifNull(anotherNonNullString)
 *       someNullHexString.ifNull{ someByteArray.toHexString() }
 */
fun <T> T?.ifNull(value: T): T = this ?: value

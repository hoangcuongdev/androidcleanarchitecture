import org.jlleitschuh.gradle.ktlint.reporter.ReporterType
buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath(GradleOldWayPlugins.ANDROID_GRADLE)
        classpath(GradleOldWayPlugins.KOTLIN_GRADLE_PLUGIN)
    }
}

plugins {
    id(GradlePluginId.DETEKT) version GradlePluginVersion.DETEKT
    id(GradlePluginId.SPOTLESS) version GradlePluginVersion.SPOTLESS
    id(GradlePluginId.ktlint) version GradlePluginVersion.KTLINT
}

allprojects {
    repositories {
        google()
        jcenter()
    }
}

subprojects {
    apply(plugin = GradlePluginId.DETEKT)
    apply(plugin = GradlePluginId.ktlint)
    apply(plugin = GradlePluginId.SPOTLESS)
    tasks.withType<Test> {
        maxParallelForks = (Runtime.getRuntime().availableProcessors() / 2).takeIf { it > 0 } ?: 1
    }

    ktlint {
        ignoreFailures.set(true)
        debug.set(true)
        android.set(true)
        enableExperimentalRules.set(true)
        filter {
            exclude("**/generated/**")
            include("**/kotlin/**")
        }
        reporters {
            reporter(ReporterType.PLAIN)
            reporter(ReporterType.CHECKSTYLE)
        }
    }

    spotless {
        isEnforceCheck = false
        kotlin {
            trimTrailingWhitespace()
            endWithNewline()
            ktlint()
        }
        kotlinGradle {
            ktlint()
        }
    }

    detekt {
        config = files("${project.rootDir}/detekt.yml")
        parallel = true
    }
}

tasks {
    withType<io.gitlab.arturbosch.detekt.Detekt> {
        // Target version of the generated JVM bytecode. It is used for type resolution.
        this.jvmTarget = "1.8"
    }
}

// tasks {
//    val clean by registering(Delete::class) {
//        delete(buildDir)
//    }
// }

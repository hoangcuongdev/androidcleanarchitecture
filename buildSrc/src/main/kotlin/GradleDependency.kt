object GradlePluginVersion {
    const val ANDROID_GRADLE = "3.6.3"
    const val KTLINT = "9.2.1"
    const val DETEKT = "1.5.0"
    const val GRADLE_VERSION_PLUGIN = "0.22.0"
    const val SPOTLESS = "3.28.1"
    const val KOTLIN = CoreVersion.KOTLIN
    const val SAFE_ARGS = CoreVersion.NAVIGATION
}

object GradlePluginId {
    const val DETEKT = "io.gitlab.arturbosch.detekt"
    const val SPOTLESS = "com.diffplug.gradle.spotless"
    const val ktlint = "org.jlleitschuh.gradle.ktlint"
    const val ANDROID_APPLICATION = "com.android.application"
    const val ANDROID_DYNAMIC_FEATURE = "com.android.dynamic-feature"
    const val ANDROID_LIBRARY = "com.android.library"
    const val KOTLIN_JVM = "org.jetbrains.kotlin.jvm"
    const val KOTLIN_ANDROID = "kotlin-android"
    const val KOTLIN_ANDROID_EXTENSIONS = "kotlin-android-extensions"
    const val GRADLE_VERSION_PLUGIN = "com.github.ben-manes.versions"
    const val SAFE_ARGS = "androidx.navigation.safeargs.kotlin"
}

object GradleOldWayPlugins {
    const val ANDROID_GRADLE = "com.android.tools.build:gradle:${GradlePluginVersion.ANDROID_GRADLE}"
    const val KOTLIN_GRADLE_PLUGIN = "org.jetbrains.kotlin:kotlin-gradle-plugin:${GradlePluginVersion.KOTLIN}"
    const val SAFE_ARGS = "androidx.navigation:navigation-safe-args-gradle-plugin:${GradlePluginVersion.SAFE_ARGS}"
    const val KTLINT = "com.pinterest:ktlint:${GradlePluginVersion.KTLINT}"
}

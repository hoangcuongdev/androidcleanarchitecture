object Releases {
    private const val majorVersion = 0 //MAJOR version when you make incompatible API changes
    private const val minorVersion = 0 //MINOR version when you add functionality in a backwards-compatible manner
    private const val patchVersion = 1 //PATCH version when you make backwards-compatible bug fixes.
    private const val screenSize = 0 //screen sizes or GL texture formats
    const val isSnapshot = false
    const val versionName = "$majorVersion.$minorVersion.$patchVersion"
    const val versionNameSnapshot = "$majorVersion.$minorVersion.$patchVersion-SNAPSHOT"

    //The greatest value Google Play allows for versionCode is 2100000000.
    const val versionCode = 1_0_00_00_00 * AndroidConfig.MIN_SDK_VERSION + 1_00_00_00 * screenSize + 1_00_00 * majorVersion + 1_00 * minorVersion + patchVersion
}
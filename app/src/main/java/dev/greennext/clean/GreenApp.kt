package dev.greennext.clean

import android.app.Application
import com.facebook.stetho.Stetho
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule

class GreenApp : Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@GreenApp))
    }

    override fun onCreate() {
        super.onCreate()
        initStetho()
    }

    private fun initStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }
}

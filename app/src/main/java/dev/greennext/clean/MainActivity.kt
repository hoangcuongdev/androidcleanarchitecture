package dev.greennext.clean

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.ActivityResult
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        appUpdateManager = AppUpdateManagerFactory.create(this)

        appUpdateManager.appUpdateInfo.addOnSuccessListener {
            if (it.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE &&
                it.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)
            ) { //  check for the type of update flow you want
                requestUpdate(it)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_FLEXIBLE_UPDATE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    //  handle user's approval
                }
                Activity.RESULT_CANCELED -> {
                    //  handle user's rejection
                }
                ActivityResult.RESULT_IN_APP_UPDATE_FAILED -> {
                    //  handle update failure
                }
            }
        }
    }

    private fun requestUpdate(appUpdateInfo: AppUpdateInfo?) {
        appUpdateManager.startUpdateFlowForResult(
            appUpdateInfo,
            AppUpdateType.FLEXIBLE, //  HERE specify the type of update flow you want
            this, //  the instance of an activity
            REQUEST_CODE_FLEXIBLE_UPDATE
        )
    }

    private lateinit var appUpdateManager: AppUpdateManager

    companion object {
        private const val REQUEST_CODE_FLEXIBLE_UPDATE = 17362
    }
}

package dev.greennext.core.data.di

import dev.greennext.core.data.remote.API
import dev.greennext.core.data.utils.createClient
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

internal val networkModule = Kodein.Module("NetworkModule") {

    bind() from singleton {
        Retrofit.Builder()
//            .baseUrl(BuildConfig.BASE_URL)
            .client(createClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    bind() from singleton { instance<Retrofit>().create(API::class.java) }
}

package dev.greennext.core.data.utils

import com.facebook.stetho.okhttp3.StethoInterceptor
import dev.greennext.core.BuildConfig
import dev.greennext.core.data.remote.HeaderInterceptor
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

private const val CONNECT_TIMEOUT = 20L
private const val READ_TIMEOUT = 30L
private const val WRITE_TIMEOUT = 30L

internal fun createClient(): OkHttpClient {
    val okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
    okHttpClientBuilder.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
    okHttpClientBuilder.writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
    okHttpClientBuilder.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
    if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        okHttpClientBuilder.addInterceptor(loggingInterceptor).addNetworkInterceptor(
            StethoInterceptor()
        )
    }
    okHttpClientBuilder.addInterceptor(HeaderInterceptor())
    return okHttpClientBuilder.build()
}

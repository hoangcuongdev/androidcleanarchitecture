package dev.greennext.core.data.di

import org.kodein.di.Kodein

internal val repositoryModule = Kodein.Module("RepositoryModule") {
}

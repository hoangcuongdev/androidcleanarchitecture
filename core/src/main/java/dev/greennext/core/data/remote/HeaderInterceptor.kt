package dev.greennext.core.data.remote

import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor : Interceptor {
    companion object {
        private const val ACCEPT_HEADER = "Content-Type"
        private const val JSON_TYPE = "application/json"
    }
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder()
            .addHeader(ACCEPT_HEADER, JSON_TYPE)
            .build()
        return chain.proceed(request)
    }
}

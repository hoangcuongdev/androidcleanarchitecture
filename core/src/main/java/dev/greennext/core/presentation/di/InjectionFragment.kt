package dev.greennext.core.presentation.di

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import dev.greennext.core.BuildConfig
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinTrigger
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.kcontext

internal abstract class InjectionFragment : Fragment(), KodeinAware {
    override val kodeinContext = kcontext(this)

    override val kodein: Kodein by kodein()

    override val kodeinTrigger: KodeinTrigger?
        get() = if (BuildConfig.DEBUG) KodeinTrigger() else super.kodeinTrigger

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        kodeinTrigger?.trigger()
    }
}

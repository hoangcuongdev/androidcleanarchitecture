package dev.greennext.core.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import dev.greennext.core.presentation.di.InjectionFragment

internal abstract class BaseFragment : InjectionFragment() {
    @get:LayoutRes
    protected abstract val layoutResId: Int

    protected abstract fun titleName(): String

    protected abstract fun initView()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutResId, container, false)
    }
}

package dev.greennext.core.presentation

import android.os.Bundle
import androidx.annotation.LayoutRes
import dev.greennext.core.presentation.di.InjectionActivity

internal abstract class BaseActivity : InjectionActivity() {
    @get:LayoutRes
    protected abstract val layoutResId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)
    }
}

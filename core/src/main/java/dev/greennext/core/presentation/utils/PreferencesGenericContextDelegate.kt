@file:JvmName("PreferencesGenericContextDelegate")
package dev.greennext.core.presentation.utils

import android.content.Context
import android.content.SharedPreferences
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

private const val APP_PREF_NAME = "APP_PREF_NAME"

private class PreferenceWithContextProperty<T>(
    private val key: String,
    private val defaultValue: T,
    private val getter: SharedPreferences.(String, T) -> T,
    private val setter: SharedPreferences.Editor.(String, T) -> SharedPreferences.Editor
) : ReadWriteProperty<Context, T> {

    override fun getValue(thisRef: Context, property: KProperty<*>): T =
        thisRef.getPreferences()
            .getter(key, defaultValue)

    override fun setValue(thisRef: Context, property: KProperty<*>, value: T) =
        thisRef.getPreferences()
            .edit()
            .setter(key, value)
            .apply()

    private fun Context.getPreferences(): SharedPreferences =
        getSharedPreferences(APP_PREF_NAME, Context.MODE_PRIVATE)
}

fun intPreference(key: String, defaultValue: Int = 0): ReadWriteProperty<Context, Int> =
    PreferenceWithContextProperty(
        key = key,
        defaultValue = defaultValue,
        getter = SharedPreferences::getInt,
        setter = SharedPreferences.Editor::putInt
    )

fun booleanPreference(key: String, defaultValue: Boolean = false): ReadWriteProperty<Context, Boolean> =
    PreferenceWithContextProperty(
        key = key,
        defaultValue = defaultValue,
        getter = SharedPreferences::getBoolean,
        setter = SharedPreferences.Editor::putBoolean
    )

fun stringPreference(key: String, defaultValue: String? = null): ReadWriteProperty<Context, String?> =
    PreferenceWithContextProperty(
        key = key,
        defaultValue = defaultValue,
        getter = SharedPreferences::getString,
        setter = SharedPreferences.Editor::putString
    )

fun stringSetPreference(key: String, defaultValue: Set<String>? = null): ReadWriteProperty<Context, Set<String>?> =
    PreferenceWithContextProperty(
        key = key,
        defaultValue = defaultValue,
        getter = SharedPreferences::getStringSet,
        setter = SharedPreferences.Editor::putStringSet
    )

fun longPreference(key: String, defaultValue: Long = 0L): ReadWriteProperty<Context, Long> =
    PreferenceWithContextProperty(
        key = key,
        defaultValue = defaultValue,
        getter = SharedPreferences::getLong,
        setter = SharedPreferences.Editor::putLong
    )

fun floatPreference(key: String, defaultValue: Float = 0.0F): ReadWriteProperty<Context, Float> =
    PreferenceWithContextProperty(
        key = key,
        defaultValue = defaultValue,
        getter = SharedPreferences::getFloat,
        setter = SharedPreferences.Editor::putFloat
    )

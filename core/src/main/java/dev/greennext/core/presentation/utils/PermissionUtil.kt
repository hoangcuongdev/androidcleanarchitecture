@file:JvmName("PermissionUtil")
package dev.greennext.core.presentation.utils

import android.Manifest.permission.CAMERA
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE

// Usage cameraPermission.isPermissionsGranted(context)
val cameraPermission = arrayOf(CAMERA, WRITE_EXTERNAL_STORAGE)

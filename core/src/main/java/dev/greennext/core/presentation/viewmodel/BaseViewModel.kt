package dev.greennext.core.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.greennext.core.extension.toLiveData
import kotlin.properties.Delegates

internal abstract class BaseViewModel<ViewState : BaseViewState,
    ViewAction : BaseAction>(initialState: ViewState) :
    ViewModel() {

    private val stateMutableLiveData = MutableLiveData<ViewState>()
    protected val stateLiveData = stateMutableLiveData.toLiveData()

    // Delegate will handle state event deduplication
    // (multiple states of the same type holding the same data will not be dispatched multiple times to LiveData stream)
    protected var state by Delegates.observable(initialState) { _, old, new ->
        stateMutableLiveData.value = new
    }

    protected fun sendAction(viewAction: ViewAction) {
        state = onReduceState(viewAction)
    }

    protected fun loadData() {
        onLoadData()
    }

    protected open fun onLoadData() {}

    protected abstract fun onReduceState(viewAction: ViewAction): ViewState
}

package dev.greennext.core.di

import org.kodein.di.Kodein

interface KodeinModuleProvider {
    val kodeinModule: Kodein.Module
}

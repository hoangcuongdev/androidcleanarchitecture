package dev.greennext.core.extension

import androidx.fragment.app.*
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) =
    beginTransaction().func().commit()

inline fun <reified T : ViewModel> Fragment.viewModel(
    factory: ViewModelProvider.Factory,
    body: T.() -> Unit
): T {
    val vm by viewModels<T> { factory }
    vm.body()
    return vm
}
inline fun <reified T : ViewModel> Fragment.activityViewModel(
    factory: ViewModelProvider.Factory,
    body: T.() -> Unit
): T {
    val vm by activityViewModels<T> { factory }
    vm.body()
    return vm
}

package dev.greennext.core.extension

import android.os.Build
import dev.greennext.core.BuildConfig

fun Any.justTryCatch(
    tryBlock: () -> Unit,
    catchBlock: ((t: Throwable) -> Unit)? = null,
    finalBlock: (() -> Unit)? = null
) {
    try {
        tryBlock()
    } catch (e: Exception) {
        catchBlock?.invoke(e)
    } finally {
        finalBlock?.invoke()
    }
}

/**
 * execute "body" if app in debug mode
 */
inline fun debug(body: () -> Unit) {
    if (BuildConfig.DEBUG) body.invoke()
}

/**
 * execute "block" if current version is greater than
 * or equal to given version
 */
inline fun supportsVersion(ver: Int, block: () -> Unit) {
    if (Build.VERSION.SDK_INT >= ver) {
        block.invoke()
    }
}

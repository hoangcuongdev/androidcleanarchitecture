package dev.greennext.core.extension

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import androidx.core.location.LocationManagerCompat
import ifNull

fun Context.isLocationEnabled(): Boolean {
    val locationManager = this.getSystemService(Context.LOCATION_SERVICE) as? LocationManager
    return locationManager?.let { LocationManagerCompat.isLocationEnabled(locationManager) }.ifNull(false)
}

inline fun <reified T : Activity> Context.open() =
    startActivity(Intent(this, T::class.java))

package dev.greennext.core.extension

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat

// Permission check extension
fun Array<String>.isPermissionsGranted(context: Context): Boolean {
    for (i in this.indices) {
        if (ActivityCompat.checkSelfPermission(context, this[i]) !=
            PackageManager.PERMISSION_GRANTED
        ) {
            return false
        }
    }
    return true
}

package dev.greennext.core.extension

import androidx.viewpager.widget.ViewPager

fun ViewPager.canGoBack() = currentItem > 0

// fun ViewPager.canGoNext() = adapter != null && currentItem < adapter!!.count - 1
fun ViewPager.canGoNext() = adapter?.takeIf { currentItem < it.count - 1 } != null

fun ViewPager.goPrevious() {
    if (canGoBack()) currentItem -= 1
}

fun ViewPager.goNext() {
    if (canGoNext()) currentItem += 1
}

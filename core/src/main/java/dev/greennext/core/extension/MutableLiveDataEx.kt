package dev.greennext.core.extension

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

internal fun <T> MutableLiveData<T>.toLiveData() = this as LiveData<T>

package dev.greennext.core.extension

import android.annotation.SuppressLint
import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter

@SuppressLint("WrongConstant")
fun RecyclerView.setUp(
    context: Context,
    orientation: Int = LinearLayoutManager.VERTICAL,
    adapter: Adapter<*>?
) {
    val layoutManager = LinearLayoutManager(context)
    layoutManager.orientation = orientation
    this.layoutManager = layoutManager
    this.setHasFixedSize(true)
    adapter?.let {
        this.adapter = adapter
    }
}
